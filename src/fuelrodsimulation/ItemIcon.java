/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public enum ItemIcon {
	CONTROL_ROD(ControlRod::renderStatic, ControlRod::create),
	MOVABLE_CONTROL_ROD(MovableControlRod::renderStatic, MovableControlRod::create),
	FUEL_ROD(FuelRod::render, FuelRod::create),
	WATER(Water::render, Water::create),
	REFLECTOR(Reflector::render, Reflector::create);

	private final Renderer renderer;
	private final Creator creator;

	private ItemIcon(Renderer renderer, Creator creator) {
		this.renderer = renderer;
		this.creator = creator;
	}

	public Renderer getRenderer() {
		return renderer;
	}

	public ItemIcon next() {
		int id = ordinal();
		ItemIcon[] values = values();
		id++;
		if (id == values.length) {
			return values[0];
		} else {
			return values[id];
		}
	}

	public ItemIcon prev() {
		int id = ordinal();
		ItemIcon[] values = values();
		if (id == 0) {
			return values[values.length - 1];
		} else {
			return values[id - 1];
		}
	}

	public Item create(RunningState state, int x, int y) {
		return creator.create(state, x, y);
	}

	public interface Renderer {

		void render(Context context, Graphics graphics,
				int x, int y) throws SlickException;
	}

	public interface Creator {

		Item create(RunningState state, int x, int y);
	}
}
