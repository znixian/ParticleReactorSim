/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public abstract class AdjuctableItem extends Item {

	protected Color fill;
	protected float amount;

	public AdjuctableItem(RunningState state, int x, int y) {
		super(state, x, y);
		adjustFill();
	}

	@Override
	public void keyPressed(int key) {
		switch (key) {
			case Input.KEY_Q:
				amount += 0.1f;
				break;
			case Input.KEY_A:
				amount -= 0.1f;
				break;
			case Input.KEY_E:
				amount += 0.01f;
				break;
			case Input.KEY_D:
				amount -= 0.01f;
				break;
			case Input.KEY_R:
				amount = 1;
				break;
			case Input.KEY_F:
				amount = 0;
				break;
			default:
				break;
		}

		if (amount < 0) {
			amount = 0;
		}
		if (amount > 1) {
			amount = 1;
		}

		adjustFill();
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		g.setColor(fill);
		g.fillRect(getX(), getY(), getWidth(), getHeight());
		g.setColor(getOutlineColour());
		g.drawRect(getX(), getY(), getWidth(), getHeight());
	}

	protected abstract Color getOutlineColour();

	protected abstract void adjustFill();
}
