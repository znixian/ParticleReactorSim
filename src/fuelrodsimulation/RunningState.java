/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.KeyListener;
import xyz.znix.slickengine.MouseListener;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.State;
import xyz.znix.slickengine.objects.ObjectList;

/**
 *
 * @author znix
 */
public class RunningState implements State, KeyListener, MouseListener {

	private final boolean[] buttons = new boolean[255];
	private final boolean[] keys = new boolean[0xFF];
	private final ObjectList<Item> items;
	private final LinkedList<Neutron> particles;
	private final ArrayList<Neutron> toAdd;
	private final ArrayList<MovableControlRod> rods;

	public final Random random = new Random();

	private ItemIcon selected = ItemIcon.CONTROL_ROD;

	private final int[] numOfNeutrons = new int[20];
	private int lastNeutronPointer;
	private int neutronPointer;

	private int mouseX, mouseY;
	private int energy;

	public RunningState(SlickEngine engine) {
		items = new ObjectList<>();
		rods = new ArrayList<>();

		particles = new LinkedList<>();
		toAdd = new ArrayList<>();
	}

	private int c2s(int value) {
		return 50 * value;
	}

	private int s2c(int value) {
		return (value) / 50;
	}

	private int round(int value) {
		return c2s(s2c(value));
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
		// reset the energy
		energy = 0;

		items.update(context, delta);
		Random r = FuelRodSimulation.RAND;
		int num = 0;
		for (Iterator<Neutron> iterator = particles.iterator(); iterator.hasNext();) {
			Neutron next = iterator.next();
			Neutron.Action action = next.update(items, context, delta);
			if (action == Neutron.Action.REMOVE) {
				iterator.remove();
				continue;
			} else if (action == Neutron.Action.DUPLICATE) {
				for (int i = 0; i < 4; i++) {
					addNeutron(next, r);
				}
			}
			num++;
		}
//		addNeutron(new Neutron(270, 270), r);
		particles.addAll(toAdd);
		toAdd.clear();

		lastNeutronPointer = neutronPointer;
		neutronPointer++;
		neutronPointer %= numOfNeutrons.length;
		numOfNeutrons[neutronPointer] = num;
	}

	public void addNeutron(Neutron neutron, Random r) {
		int x = (int) neutron.x + r.nextInt(5);
		int y = (int) neutron.y + r.nextInt(5);
		toAdd.add(new Neutron(x, y));
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		items.render(context, g);
		GL11.glDisable(GL11.GL_TEXTURE);
		GL11.glPointSize(2);
		GL11.glDisable(GL11.GL_BLEND);

		for (int i = 0; i < 10; i++) {
			final int ii = i; // must be final for lambda
			GL11.glBegin(GL11.GL_POINTS);
			GL11.glColor3f(1, 0, i / 10f);
			particles.stream().forEach(p -> p.render(ii));
			GL11.glEnd();
		}

		GL11.glEnable(GL11.GL_BLEND);

		g.setColor(Color.white);

		int average = 0;
		for (int numOfNeutron : numOfNeutrons) {
			average += numOfNeutron;
		}
		average /= numOfNeutrons.length;

		int last = numOfNeutrons[lastNeutronPointer];
		int delta = numOfNeutrons[neutronPointer] - last;

		int y = 35;
		g.drawString("Particles: " + numOfNeutrons[neutronPointer], 10, y);
		g.drawString("AV " + average, 180, y);
		y += 20;
		g.drawString("DP: " + delta, 10, y);
		y += 20;
		g.drawString("ENG: " + energy, 10, y);

		int margin = 16;
		y = margin;
		int x = context.getWidth() - Item.getSize() - margin;
		for (ItemIcon icon : ItemIcon.values()) {
			if (icon == selected) {
				g.setColor(Color.white);
				int w = 16;
				g.fillRect(x - margin - w, y, w, Item.getSize());
			}
			icon.getRenderer().render(context, g, x, y);
			y += Item.getSize() + margin;
		}
	}

	@Override
	public void keyPressed(int key, char c) {
		keys[key] = true;
		if (key == Input.KEY_W) {
			alterRods(-1);
		} else if (key == Input.KEY_S) {
			alterRods(1);
		} else if (key >= Input.KEY_1 && key <= Input.KEY_0) {
			int id = key - Input.KEY_1;
			ItemIcon[] values = ItemIcon.values();
			if (id < values.length) {
				selected = values[id];
			}
		} else {
			Optional<Item> sel = items.stream()
					.filter(i -> i.isInside(mouseX, mouseY))
					.findFirst();

			if (sel.isPresent()) {
				Item item = sel.get();
				item.keyPressed(key);
			}
		}
	}

	@Override
	public void keyReleased(int key, char c) {
		keys[key] = false;
	}

	private void alterRods(int diff) {
		rods.stream().forEach((rod) -> {
			rod.alter(diff);
		});
	}

	@Override
	public void mouseWheelMoved(int change) {
		if (change > 0) {
			selected = selected.prev();
		} else {
			selected = selected.next();
		}
	}

	private void processMouse(int x, int y) {
		x = round(x);
		y = round(y);
		if (buttons[Input.MOUSE_LEFT_BUTTON]) {
			removeAt(x, y);
			items.add(selected.create(this, x, y));
		} else if (buttons[Input.MOUSE_RIGHT_BUTTON]) {
			removeAt(x, y);
		}
	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		mouseX = newx;
		mouseY = newy;

		if (round(newx) == round(oldx) && round(newy) == round(oldy)) {
			return;
		}
		processMouse(newx, newy);
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		mouseX = newx;
		mouseY = newy;
	}

	@Override
	public void mousePressed(int button, int x, int y) {
		buttons[button] = true;
		if (keys[Input.KEY_LSHIFT]) {
			// process without rounding mouse

			Item it = selected.create(this, x, y);
			items.add(it);

			List<Item> matched = new ArrayList<>();

			items.stream()
					.filter(i -> i.overlaps(it))
					.filter(i -> i != it)
					.forEach(matched::add);

			matched.forEach(items::remove);
		} else {
			processMouse(x, y);
		}
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		buttons[button] = false;
	}

	private void removeAt(int x, int y) {
		List<Item> matched = new ArrayList<>();

		items.stream()
				.filter(i -> i.isInside(x, y))
				.forEach(matched::add);

		matched.forEach(items::remove);
	}

	public ArrayList<MovableControlRod> getRods() {
		return rods;
	}

	public void addEnergy(int amt) {
		energy += amt;
	}

}
