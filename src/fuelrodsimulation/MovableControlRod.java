/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author znix
 */
public class MovableControlRod extends ControlRod {

	private static final int MAX_SPACING = 15;
	private int position = 0;

	public MovableControlRod(RunningState state, int x, int y) {
		super(state, x, y);
		state.getRods().add(this);
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		g.setColor(Color.lightGray);
		float amount = 1f * position / MAX_SPACING;
		g.drawRect(getX(), getY(), getWidth(), getHeight());
		g.fillRect(getX(), getY(), getWidth(), getHeight() * amount);
	}

	public static Item create(RunningState state, int x, int y) {
		return new MovableControlRod(state, x, y);
	}

	public static void renderStatic(Context context, Graphics g, int x, int y) {
		g.setColor(Color.lightGray);
		float amount = 0.5f;
		g.drawRect(x, y, getSize(), getSize());
		g.fillRect(x, y, getSize(), getSize() * amount);
	}

	@Override
	public boolean willReact(Neutron neutron) {
		int spacing = MAX_SPACING - position;
		return (int) neutron.x % spacing == 0 && (int) neutron.y % spacing == 0;
	}

	public void alter(int amount) {
		position += amount;
		if (position < 0) {
			position = 0;
		}
		if (position > MAX_SPACING - 1) {
			position = MAX_SPACING - 1;
		}
	}
}
