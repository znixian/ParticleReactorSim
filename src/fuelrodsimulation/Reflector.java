/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author znix
 */
public class Reflector extends Item {

	public Reflector(RunningState state, int x, int y) {
		super(state, x, y);
	}

	@Override
	public int getWidth() {
		return getSize();
	}

	@Override
	public int getHeight() {
		return getSize();
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		g.setColor(Color.magenta);
		g.drawRect(getX(), getY(), getWidth(), getHeight());
	}

	public static void render(Context context, Graphics g,
			int x, int y) {
		g.setColor(Color.magenta);
		g.drawRect(x, y, getSize(), getSize());
	}

	public static Item create(RunningState state, int x, int y) {
		return new Reflector(state, x, y);
	}

	@Override
	public boolean willReact(Neutron neutron) {
		return true;
	}

	@Override
	public Neutron.Action react(Neutron n) {
		float dx = n.x - x;
		float dy = n.y - y;

		boolean s = dx > dy;
		boolean o = dx + dy < getSize();

		if (s && o) {
			n.vy = -Math.abs(n.vy);
		} else if (s) {
			n.vx = Math.abs(n.vx);
		} else if (o) {
			n.vx = -Math.abs(n.vx);
		} else {
			n.vy = Math.abs(n.vy);
		}

		return Neutron.Action.NOAC;
	}

}
