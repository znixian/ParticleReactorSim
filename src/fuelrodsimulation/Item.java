/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import fuelrodsimulation.Neutron.Action;
import xyz.znix.slickengine.objects.BasicObject;

/**
 *
 * @author znix
 */
public abstract class Item extends BasicObject {

	protected final RunningState state;

	public Item(RunningState state, int x, int y) {
		this.state = state;
		this.x = x;
		this.y = y;
	}

	public Item(RunningState state) {
		this.state = state;
	}

	public static int getSize() {
		return 48;
	}

	public abstract boolean willReact(Neutron neutron);

	public abstract Action react(Neutron neutron);

	public void keyPressed(int key) {
	}
}
