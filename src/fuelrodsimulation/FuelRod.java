/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import java.util.Random;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author znix
 */
public class FuelRod extends AdjuctableItem {

	private static final Color COLOUR = Color.cyan;

	public static final int MAX_SPLIT_POWER = 450;
	public static final int MIN_SPLIT_POWER = 200;
	public static final int REACT_TIME = 100;

	private int timeUpdate;
	private boolean duping;

	public FuelRod(RunningState state, int x, int y) {
		super(state, x, y);
		amount = 0.25f;
	}

	@Override
	public int getWidth() {
		return 32;
	}

	@Override
	public int getHeight() {
		return 32;
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
		timeUpdate += delta;
		duping = false;
		if (timeUpdate > REACT_TIME) {
			duping = true;
			timeUpdate -= REACT_TIME;
			Random r = FuelRodSimulation.RAND;
			state.addNeutron(new Neutron(
					getX() + r.nextInt(getWidth()),
					getY() + r.nextInt(getHeight())
			), r);
		}
	}

	public static void render(Context context, Graphics g, int x, int y) {
		g.setColor(COLOUR);
		g.drawRect(x, y, getSize(), getSize());
	}

	public static Item create(RunningState state, int x, int y) {
		return new FuelRod(state, x, y);
	}

	@Override
	public boolean willReact(Neutron neutron) {
		if (!(MIN_SPLIT_POWER < neutron.energy && neutron.energy < MAX_SPLIT_POWER)) {
			return false;
		}
		if (state.random.nextFloat() < amount) {
			return duping;
		} else {
			return false;
		}
	}

	@Override
	public Neutron.Action react(Neutron neutron) {
		return Neutron.Action.DUPLICATE;
	}

	@Override
	protected Color getOutlineColour() {
		return COLOUR;
	}

	@Override
	protected void adjustFill() {
		fill = new Color(0, 1, 1, amount);
	}

}
