/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author znix
 */
public class Water extends AdjuctableItem {

	private static final Color outline = Color.blue;

	public Water(RunningState state, int x, int y) {
		super(state, x, y);
	}

	@Override
	public int getWidth() {
		return getSize();
	}

	@Override
	public int getHeight() {
		return getSize();
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
	}

	public static void render(Context context, Graphics g, int x, int y) {
		g.setColor(outline);
		g.drawRect(x, y, getSize(), getSize());
	}

	public static Item create(RunningState state, int x, int y) {
		return new Water(state, x, y);
	}

	@Override
	public boolean willReact(Neutron neutron) {
		return state.random.nextFloat() < amount;
	}

	@Override
	public Neutron.Action react(Neutron neutron) {
		state.addEnergy(1);
		return Neutron.Action.REMOVE;
	}

	@Override
	protected Color getOutlineColour() {
		return outline;
	}

	@Override
	protected void adjustFill() {
		fill = new Color(0, 0, 1, amount);
	}

}
