/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import org.lwjgl.opengl.GL11;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.objects.ObjectList;

/**
 *
 * @author znix
 */
public final class Neutron {

	public static final float INIT_ENERGY = 500;

	public float x, y;
	public float vx, vy;
	public float energy;

	public Neutron(float x, float y) {
		this.x = x;
		this.y = y;
		this.energy = INIT_ENERGY;
		double direction = FuelRodSimulation.RAND.nextDouble() * Math.PI * 2;
		vx = (float) (Math.cos(direction));
		vy = (float) (Math.sin(direction));
	}

	public Action update(ObjectList<Item> objects, Context context, int delta) {
		if (x < 0) {
			return Action.REMOVE;
		}
		if (y < 0) {
			return Action.REMOVE;
		}
		if (x > context.getWidth()) {
			return Action.REMOVE;
		}
		if (y > context.getHeight()) {
			return Action.REMOVE;
		}

		energy -= delta / 5;
		if (energy <= 0) {
			return Action.REMOVE;
		}

		float speed = energy * delta / 1000;

		x += vx * speed;
		y += vy * speed;
		for (Item object : objects) {
			if (object.isInside((int) x, (int) y) && object.willReact(this)) {
				return object.react(this);
			}
		}
		return Action.NOAC;
	}

	public void render(int layer) {
		int thisLayer = (int) (energy / INIT_ENERGY * 10);
		if (thisLayer >= 10) {
			thisLayer = 9;
		}
		if (layer == thisLayer) {
			GL11.glVertex2f(x, y);
		}
	}

	public static enum Action {
		NOAC, REMOVE, DUPLICATE;
	}
}
