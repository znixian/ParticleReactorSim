/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author znix
 */
public class ControlRod extends Item {

	public ControlRod(RunningState state, int x, int y) {
		super(state, x, y);
	}

	@Override
	public int getWidth() {
		return 50;
	}

	@Override
	public int getHeight() {
		return 50;
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		g.setColor(Color.gray);
		g.drawRect(getX(), getY(), getWidth(), getHeight());
	}

	public static void renderStatic(Context context, Graphics g,
			int x, int y) {
		g.setColor(Color.gray);
		g.drawRect(x, y, getSize(), getSize());
	}

	public static Item create(RunningState state, int x, int y) {
		return new ControlRod(state, x, y);
	}

	@Override
	public boolean willReact(Neutron neutron) {
		return true;
	}

	@Override
	public Neutron.Action react(Neutron neutron) {
		return Neutron.Action.REMOVE;
	}

}
