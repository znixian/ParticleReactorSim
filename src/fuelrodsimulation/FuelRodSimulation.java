/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuelrodsimulation;

import java.util.Random;
import xyz.znix.slickengine.SlickEngine;

/**
 *
 * @author znix
 */
public class FuelRodSimulation {

	public static final Random RAND = new Random();

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		SlickEngine engine = new SlickEngine("Fuel Rods");
		engine.addState(new RunningState(engine));
		engine.startFullscreen();
	}

}
